================
Kwant extensions
================

Going beyond the core package, there are several tools that extend Kwant in useful ways.
You can check these out below.

If you have some re-usable code that you think would be useful to the wider Kwant community,
post to the `Kwant development mailing list <mailto:kwant-devel@kwant-project.org>`_ with a
link to the code and a couple of sentences describing it, and we'll add it to this page.


Semicon: k·p simulations made easy
----------------------------------
**Get the code**: https://gitlab.kwant-project.org/r-j-skolasinski/semicon

A package of tools for doing k·p simulations. Contains: model definitions, material parameters,
and helper functions for generating template Kwant systems.

Generating quasicrystals
------------------------
**Get the code**: https://arxiv.org/src/1510.06035v2/anc/quasicrystal.py

Code for reproducing numerics from the paper "`Aperiodic Weak Topological Superconductors <https://arxiv.org/abs/1510.06035>`_"
by Fulga et al.
Contains functionality for building 2D Ammann-Beenker tilings, an example of a quasicrystal, and
Kwant systems constructed from such tilings.

Time-dependent transport
------------------------
**Get the code**: https://gitlab.kwant-project.org/jbweston/tkwant

A package for defining time-dependent systems with Kwant, and calculating
time-dependent quantities. Details of the algorithm can be found in the
following two papers:

+ `Towards realistic time-resolved simulations of quantum devices <https://dx.doi.org/10.1007/s10825-016-0855-9>`_
+ `Numerical simulations of time resolved quantum electronics <https://dx.doi.org/10.1016/j.physrep.2013.09.001>`_
